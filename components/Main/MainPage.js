import React from "react";

export default function MainPage() {
    return (
        <div className={"mainpage"}>
            <div className={"wrapper"}>
                <div className={"block1"}>
                    Проанализируйте свою прибыль!

                    <p>
                        Аналитическая стастистка поможет Вам просмотреть данные на текущий год и сравнить показатели с прошлым годом
                    </p>
                </div>
                <div className={"block2"}>
                    <img src={"/images/2.png"} alt={""}/>
                </div>
            </div>
        </div>
    )
}