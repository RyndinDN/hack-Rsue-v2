import React, {useState} from 'react'
import Head from "next/head";
import {Breadcrumb, Layout, Menu} from "antd";
import SiderDemo from "../components/Main/Navbar";
import MenuUnfoldOutlined from "@ant-design/icons/lib/icons/MenuUnfoldOutlined";
import MenuFoldOutlined from "@ant-design/icons/lib/icons/MenuFoldOutlined";
import MainPage from "../components/Main/MainPage";

export default function Index() {
    const {Header, Content, Footer} = Layout;

    return (
        <>
            <Head><title>Анализ Вашей Прибыли</title></Head>
            <Layout style={{minHeight: '100vh'}}>
                <SiderDemo/>
                <Layout className="site-layout">
                    {/*<Header className="header" style={{padding: 0}}/>*/}
                    <Content style={{margin: '5px 16px'}}>
                        {/*<Breadcrumb style={{margin: '16px 0'}}>*/}
                        {/*    <Breadcrumb.Item>Старт</Breadcrumb.Item>*/}
                            {/*<Breadcrumb.Item>Bill</Breadcrumb.Item>*/}
                        {/*</Breadcrumb>*/}
                        <div className="site-layout-background" style={{padding: 24, minHeight: 360}}>
                            <MainPage/>
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>by CyberFarsh</Footer>
                </Layout>
            </Layout>
        </>

    )
}

