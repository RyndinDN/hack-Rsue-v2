import 'antd/dist/antd.css';
import '../styles/style.css'

function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default App
